@extends('dashboard')
 
@section('contenu')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> Liste des Annonces</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('annonces.create') }}"> Nouvelle</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-striped table-bordered table-hover table-sm">
        <thead class="bg-dark text-white">
            <tr> 
                <th>ID</th>
                <th>Typologie habitat</th>
                <th>Lieu</th>
                <th>Superficie</th>
                <th>Mise à prix (FCFA)</th>
                <th colspan="10">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $key => $value)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $value->type }}</td>
                    <td>{{ $value->lieu }}</td>
                    <td>{{ $value->superficie }} m<sup>2</sup></td>
                    <td>{{ $value->prix }} FCFA</td>
                    <td>
                        <form action="{{ route('annonces.destroy',$value->id) }}" method="annonce">   
                            <a class="btn btn-info" href="{{ route('annonces.show',$value->id) }}">Afficher</a>    
                            <a class="btn btn-primary" href="{{ route('annonces.edit',$value->id) }}">Modifier</a>   
                            @csrf
                            @method('Supprimer')      
                            <button type="submit" class="btn btn-danger">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>  

    {!! $data->links() !!}      
@endsection