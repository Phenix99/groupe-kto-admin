@extends('dashboard')
  
@section('contenu')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> Détails Annonce</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('annonces.index') }}"> Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Type :</strong>
                {{ $annonce->type }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Lieu :</strong>
                {{ $annonce->lieu }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Superficie (m<sup>2</sup>) :</strong>
                {{ $annonce->superficie }}m<sup>2</sup>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Prix (FCFA) :</strong>
                {{ $annonce->prix }} FCFA
            </div>
        </div>
    </div>
@endsection