@extends('dashboard')
   
@section('contenu')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Modifier Annonce</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('annonces.index') }}"> Retour</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('annonces.update',$annonce->id) }}" method="post">
        @csrf
        @method('PUT')
   
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Type :</strong>
                    <input type="text" value="{{ $annonce->type }}" name="type" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Lieu :</strong>
                    <input type="text" value="{{ $annonce->lieu }}" name="lieu" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Superficie (m<sup>2</sup>) :</strong>
                    <input type="text" value="{{ $annonce->superficie }}" name="superficie" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Prix (FCFA) :</strong>
                    <input type="text" value="{{ $annonce->prix }}" name="prix" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
   
    </form>
@endsection