@extends('dashboard')
  
@section('contenu')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Ajout Annonce</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('annonces.index') }}"> Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('annonces.store') }}" method="post">
    @csrf
  
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Type :</strong>
                <input type="text" name="type" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Lieu :</strong>
                <input type="text" name="lieu" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Superficie (m<sup>2</sup>) :</strong>
                <input type="text" name="superficie" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Prix (FCFA) :</strong>
                <input type="text" name="prix" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
        </div>
    </div>
   
</form>
@endsection